#include <atmel_start.h>
#include "ds3231_func.h"
#include "string.h"
#define SUCCESS 0
#define MAX_BUFFER_LENGTH 100

uint8_t hex_to_string(uint8_t * buffer, uint8_t * buffer_2);
uint8_t ds3231_string_to_time_string(uint8_t * buffer);

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	struct io_descriptor *I2C_0_io;
	uint8_t bufferino[100]= {};
	uint8_t bufferghini[2 * MAX_BUFFER_LENGTH] = {};
	
	i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
	i2c_m_sync_enable(&I2C_0);
	i2c_m_sync_set_slaveaddr(&I2C_0, 0x68, I2C_M_SEVEN);
	
	struct io_descriptor *io;
	usart_sync_get_io_descriptor(&USART_0, &io);
	usart_sync_enable(&USART_0);

	/* Set time */
	#if 0
	uint8_t segundos = 0x00, minutos = 0x12, horas = 0x12;
	ds3231_set_time(I2C_0_io, &segundos, &minutos, &horas);
	#endif

	/* Set day */
	#if 1
	uint8_t day_of_week = 0x05;
	ds3231_set_day(I2C_0_io, &day_of_week);
	#endif

	/* Set date */
	#if 0
	uint8_t dia = 0x03, mes = 0x05, agno = 0x19;
	ds3231_set_date(I2C_0, &dia, &mes, &agno);
	#endif

	///* Seconds */
	//io_write(I2C_0_io, (uint8_t *)"\0\0" , 2);
	///* Minutes */
	//uint8_t min_buffer[2] = {0x01,0x42};
	//io_write(I2C_0_io, min_buffer, 2);
	///* Hours */
	//uint8_t hou_buffer[2] = {0x02,0x08};
	//io_write(I2C_0_io, hou_buffer, 2);
	
	/* Replace with your application code */
	while (1) {
		delay_ms(900);
		gpio_set_pin_level(LEDCITo,true);
		//delay_ms(15);
		gpio_set_pin_level(LEDCITo,false);

		ds3231_get_all(I2C_0_io, bufferino);
		hex_to_string(bufferino, bufferghini);
		ds3231_string_to_time_string(bufferghini);
		io_write(io, (uint8_t *)bufferghini, 20);
		io_write(io, (uint8_t *)"\r\n", 2);
		
	}
}

uint8_t hex_to_string(uint8_t * buffer, uint8_t	* buffer_2)
{
#if 1
	uint8_t string_length = strlen(buffer);
	uint8_t temp_msb = 0;
	uint8_t temp_lsb = 0;
	uint8_t cont = 0, i = 0;
	for(i = 0; i < 7; i++)
	{
		temp_msb = ((buffer[i] & 0xF0) >> 4) + 0x30;
		*(buffer_2 + cont) = temp_msb;
		cont++;
		temp_lsb = (buffer[i] & 0x0F) + 0x30;
		*(buffer_2 + cont) = temp_lsb;
		cont++;
	}
#endif
#if 0
	delay_ms(1000);
	gpio_set_pin_level(LEDCITo,true);
	delay_ms(15);
	gpio_set_pin_level(LEDCITo,false);
#endif
	return SUCCESS;
	
}

uint8_t ds3231_string_to_time_string(uint8_t * buffer)
{
	uint8_t temp_hora_0 = buffer[4];
	uint8_t temp_hora_1 = buffer[5];
	uint8_t temp_min_0 = buffer[2];
	uint8_t temp_min_1 = buffer[3];
	uint8_t temp_sec_0 = buffer[0];
	uint8_t temp_sec_1 = buffer[1];
	
	uint8_t temp_dow_0 = buffer[6];
	uint8_t temp_dow_1 = buffer[7];
	
	uint8_t temp_year_0 = buffer[12];
	uint8_t temp_year_1 = buffer[13];
	uint8_t temp_mon_0 = buffer[10];
	uint8_t temp_mon_1 = buffer[11];
	uint8_t temp_day_0 = buffer[8];
	uint8_t temp_day_1 = buffer[9];
	
	*(buffer) = temp_hora_0;
	*(buffer + 1) = temp_hora_1;
	
	*(buffer + 2) = 0x3A;
	
	*(buffer + 3) = temp_min_0;
	*(buffer + 4) = temp_min_1;
	
	*(buffer + 5) = 0x3A;
	
	*(buffer + 6) = temp_sec_0;
	*(buffer + 7) = temp_sec_1;
	
	*(buffer + 8) = 0x20;
	
	*(buffer + 9) = temp_dow_0;
	*(buffer + 10) = temp_dow_1;
	
	*(buffer + 11) = 0x20;
	
	*(buffer + 12) = temp_day_0;
	*(buffer + 13) = temp_day_1;
	
	*(buffer + 14) = 0x2F;
	
	*(buffer + 15) = temp_mon_0;
	*(buffer + 16) = temp_mon_1;
	
	*(buffer + 17) = 0x2F;
	
	*(buffer + 18) = temp_year_0;
	*(buffer + 19) = temp_year_1;
	
	return SUCCESS;	
}