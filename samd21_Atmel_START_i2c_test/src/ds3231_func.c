/******************************************************************************
 * @ file ds3231_func.c
 *
 *  Created on: 03/05/2019
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include <atmel_start.h>
#include "ds3231_func.h"

/* Definitions */
#define SUCCESS 0

/* Functions */
uint8_t ds3231_set_time(struct io_descriptor * I2C_iface, uint8_t * secs, uint8_t * mins, uint8_t * hou)
{
	/* Set time */
	/* Seconds */
	uint8_t secs_buffer[2] = {0x00, *secs};
	io_write(I2C_iface, secs_buffer , 2);
	/* Minutes */
	uint8_t min_buffer[2] = {0x01, *mins};
	io_write(I2C_iface, min_buffer, 2);
	/* Hours */
	uint8_t hou_buffer[2] = {0x02, *hou};
	io_write(I2C_iface, hou_buffer, 2);
	return SUCCESS;
}

uint8_t ds3231_set_day(struct io_descriptor * I2C_iface, uint8_t * dow)
{
	/* Set Day of the Week */
	uint8_t day_buffer[2] = {0x03, *dow};
	io_write(I2C_iface, day_buffer, 2);
	return SUCCESS;
	
}

uint8_t ds3231_set_date(struct io_descriptor * I2C_iface, uint8_t * day, uint8_t * month, uint8_t * year)
{
	/* Set date */
	/* Day */
	uint8_t day_buffer[2] = {0x04, *day};
	io_write(I2C_iface, day_buffer , 2);
	/* Month */
	uint8_t month_buffer[2] = {0x05, *month};
	io_write(I2C_iface, month_buffer , 2);
	/* Year */
	uint8_t year_buffer[2] = {0x06, *year};
	io_write(I2C_iface, year_buffer, 2);
	return SUCCESS;
	
}

uint8_t ds3231_get_all(struct io_descriptor * I2C_iface, uint8_t * buffer)
{
	io_write(I2C_iface, (uint8_t *)"\0", 1);
	io_read(I2C_iface, (uint8_t *)buffer, 7);
	return SUCCESS;
}