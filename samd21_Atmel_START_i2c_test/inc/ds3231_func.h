/*
 * XXXX.h
 *
 *  Created on: DD/MM/2019
 *      Author: AntoMota
 */

#ifndef __DS3231_FUNC_H__
#define __DS3231_FUNC_H__

/* Libraries to be included */
#include <stdint.h>

/* Function prototypes of the source file this header file references to */
uint8_t ds3231_set_time(struct io_descriptor * I2c_iface, uint8_t * secs, uint8_t * mins, uint8_t * hou);
uint8_t ds3231_set_day(struct io_descriptor * I2C_iface, uint8_t * dow);
uint8_t ds3231_set_date(struct io_descriptor * I2C_iface, uint8_t * day, uint8_t * month, uint8_t * year);
uint8_t ds3231_get_all(struct io_descriptor * I2C_iface, uint8_t * buffer);



#endif /* __DS3231_FUNC_H__ */
